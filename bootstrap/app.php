<?php
use Modules\App;

include_once __DIR__ . '/../vendor/autoload.php';

$config = require __DIR__ . '/../config/config.php';
App::setConfig($config);