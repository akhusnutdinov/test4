<?php
use Modules\Counter;

// Do 6 times per minute
for ($i = 0; $i < 6; $i++) {
    Counter::generate();
    sleep(10);
}
