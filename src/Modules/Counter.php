<?php


namespace Modules;


class Counter
{
    public static function getRefererDomain()
    {
        if (isset($_SERVER['HTTP_REFERER'])) {
            $host = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
        } else {
            $host = $_SERVER['HTTP_HOST'];
        }

        return $host;
    }


    public static function increaseCounter()
    {
        $domain = self::getRefererDomain();
        $dailyKey = sprintf('daily:%s', self::getUID());
        $dailyUniqKey = sprintf('udaily:%s:%s', date('Y-m-d'), self::getUID());
        $key = 'd:' . $domain;
        $statKey = 's:' . date('Y-m-d');
        App::getRedis()->hIncrBy($key, 'total', 1);
        if (!App::getRedis()->hExists($key, $dailyUniqKey)) {
            App::getRedis()->hIncrBy($key, 'utotal', 1);
            App::getRedis()->hIncrBy($statKey, 'u:' . $domain, 1);
        }
        App::getRedis()->hIncrBy($statKey, 't:' . $domain, 1);
        App::getRedis()->hIncrBy($key, $dailyUniqKey, 1);
        App::getRedis()->hIncrBy($key, $dailyKey, 1);
    }

    private static function getUID()
    {
        return isset($_COOKIE['__uid']) ? $_COOKIE['__uid'] : self::setUID();
    }

    private static function setUID()
    {
        $uid = uniqid();
        setcookie('__uid', $uid, time() + 3600 * 24 * 365);
        return $uid;
    }

    /**
     * @param string $domain
     */
    public static function generateImage($domain)
    {
        $im = imagecreatetruecolor(120, 40);
        $white = imagecolorallocate($im, 255, 255, 255);
        $black = imagecolorallocate($im, 0, 0, 100);
        imagefill($im, 0, 0, $black);
        $font = __DIR__ . '/../../fonts/FreeSans.ttf';
        $text = sprintf('%08d', self::getDomainCounter($domain));

        imagettftext($im, 16, 0, 10, 25, $white, $font, $text);
        $fileName = sprintf(__DIR__ . '/../../public/files/%s.png', $domain);
        imagepng($im, $fileName);
    }

    public static function outputImage()
    {
        header('Content-Type: image/png');
        $fileName = sprintf(__DIR__ . '/../../public/files/%s.png', self::getRefererDomain());
        print file_get_contents($fileName);
    }

    public static function getDomains()
    {
        $domains = App::getRedis()->keys('d:*');
        return array_map(function ($name) {
            list($_, $name) = explode(':', $name);
            return $name;
        }, $domains);
    }

    public static function generate()
    {
        //TODO: Queue e.g. RabbitMQ

        $domains = self::getDomains();
        foreach ($domains as $domain) {
            self::generateImage($domain);
        }
    }

    public static function getDomainCounter($domain)
    {
        return App::getRedis()->hGet('d:' . $domain, 'total');
    }

    public static function getStatistics()
    {
        $days = App::getRedis()->keys('s:*');
        $res = [];
        foreach ($days as $dayKey) {
            list($_, $day) = explode(':', $dayKey);
            $keys = App::getRedis()->hKeys($dayKey);
            $domains = [];
            foreach ($keys as $key) {
                list($type, $domain) = explode(':', $key);
                if (!isset($domains[$domain][$type])) {
                    $domains[$domain][$type] = 0;
                }
                $count = App::getRedis()->hGet($dayKey, $key);
                $domains[$domain][$type] = $count;
            }
            $res[$day] = $domains;
        }
        return $res;
    }
}