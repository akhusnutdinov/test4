<?php
namespace Modules;

use Storage\RedisStorage;

class App
{
    protected static $config;
    protected static $uid;
    protected static $storage;

    /**
     * @param array $config
     */
    public static function setConfig(array $config)
    {
        self::$config = $config;
    }

    /**
     * @param  string $key
     * @return array
     */
    public static function getConfig($key = '')
    {
        return isset(self::$config[$key]) ? self::$config[$key] : null;
    }

    /**
     * @return \Redis
     */
    public static function getRedis() {
        if (!self::$storage) {
            $config = self::getConfig('redis');
            self::$storage = new \Redis();
            self::$storage->connect($config['host'], $config['port']);
        }
        return self::$storage;
    }

}