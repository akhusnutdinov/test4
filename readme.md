# Requirements
- php 
- php-gd
- php-redis
- redis

# Installation
- composer update
- add `* * * * * php WORKING_DIR/scheduler.php` to crontab