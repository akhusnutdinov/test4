<?php

use Modules\Counter;

include_once __DIR__ . '/../bootstrap/app.php';

Counter::increaseCounter();
Counter::outputImage();