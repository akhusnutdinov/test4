<?php include __DIR__ . '/../bootstrap/app.php';

$stat = \Modules\Counter::getStatistics();
?>

<table>
    <tr>
        <th>Day</th>
        <th>Domain</th>
        <th>Total uniq</th>
        <th>Total</th>
    </tr>
<?php foreach ($stat as $day => $domains): ?>
    <?php foreach ($domains as $domain => $fields):?>
        <tr>
            <td><?= $day?></td>
            <td><?= $domain?></td>
            <td><?= $fields['u']?></td>
            <td><?= $fields['t']?></td>
        </tr>
    <?php endforeach ?>
<?php endforeach ?>

</table>
